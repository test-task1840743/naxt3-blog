import data from '~/api/data-posts.json';
import { postShortFactory } from '~/factory/post-short.factory';
import { postFactory } from '~/factory/post.factory';
import type { Post } from '~/entry/post';
import type { PostShort } from '~/entry/post-short';
import type { PostDto } from '~/dto/post.dto';
import type { PostCreateDto } from '~/dto/post-create.dto';
import type { PostUpdateDto } from '~/dto/post-update.dto';

let myData = [...data];
export const postApi = {
  async getAll(): Promise<PostShort[]> {
    //new Promise заменим на любой http клиент к примеру axios
    return new Promise<PostDto[]>((resolve, reject) => {
      setTimeout(() => resolve(myData as PostDto[]), 500);
    }).then((res) => res.map((el) => postShortFactory.makeFromDto(el)));
  },

  async getById(id: number): Promise<Post> {
    return new Promise<PostDto>((resolve, reject) => {
      let res = myData.find((el) => el.id == id);
      if (!res) reject('Not element');
      setTimeout(() => resolve(res as PostDto), 500);
    }).then((res) => postFactory.makeFromDto(res));
  },

  async create(dto: PostCreateDto): Promise<Post> {
    const id = Math.floor(Math.random() * 100000) + 10;
    const newDto = {
      id,
      pageContent: {
        img: 'https://dummyimage.com/1920x800/000/fff&text=fullImg',
        description: '',
        date: '24.07.2022'
      },
      posts: [1, 2, 3, 4],
      ...dto
    };
    return new Promise<PostDto>((resolve, reject) => {
      setTimeout(() => {
        myData.push(newDto);
        myData.forEach((el) => el.posts.push(id));
        resolve(newDto as PostDto);
      }, 500);
    }).then((res) => postFactory.makeFromDto(res));
  },

  async update(dto: PostUpdateDto): Promise<Post> {
    return new Promise<PostDto>((resolve, reject) => {
      setTimeout(() => {
        const postIndex = myData.findIndex((el) => el.id == dto.id);
        myData[postIndex] = {
          ...myData[postIndex],
          ...dto
        };
        resolve(myData[postIndex] as PostDto);
      }, 500);
    }).then((res) => postFactory.makeFromDto(res));
  }
};
