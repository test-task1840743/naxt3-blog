﻿import type { PostCreateDto } from '~/dto/post-create.dto';
import { postApi } from '~/api/post.api';

export const postCreateUse = () => {
  const loading = ref(false);

  const create = (dto: PostCreateDto) => {
    loading.value = true;
    return postApi.create(dto).then((res) => {
      loading.value = false;
      return res;
    });
  };

  return {
    postCreate: create,
    postCreateLoading: computed(() => loading)
  };
};
