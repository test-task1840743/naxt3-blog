import { postApi } from '~/api/post.api';
import type { PostShort } from '~/entry/post-short';
import type { ComputedRef, Ref } from '@vue/reactivity';
import type { Post } from '~/entry/post';

interface PostUse {
  post: ComputedRef<Post | null>;
  postLoading: ComputedRef<boolean>;
}

export const postUse = (userId: ComputedRef<number> | Ref<number>): PostUse => {
  const post = ref<Post | null>(null);
  const loading = ref(false);

  watchEffect(() => {
    if (userId.value) {
      loading.value = true;
      postApi
        .getById(userId.value)
        .then((res) => {
          post.value = res;
          return res;
        })
        .finally(() => {
          loading.value = false;
        });
    }
  });

  return {
    post: computed<Post | null>(() => post.value),
    postLoading: computed<boolean>(() => loading.value)
  };
};
