﻿import type { PostUpdateDto } from '~/dto/post-Update.dto';
import { postApi } from '~/api/post.api';

export const postUpdateUse = () => {
  const loading = ref(false);

  const update = (dto: PostUpdateDto) => {
    loading.value = true;
    return postApi.update(dto).then((res) => {
      loading.value = false;
      return res;
    });
  };

  return {
    postUpdate: update,
    postUpdateLoading: computed(() => loading)
  };
};
