import { postApi } from '~/api/post.api';
import type { PostShort } from '~/entry/post-short';
import type { ComputedRef } from '@vue/reactivity';

interface PostsUse {
  posts: ComputedRef<PostShort[]>;
  postsLoading: ComputedRef<boolean>;
}

export const postsUse = (): PostsUse => {
  const posts = ref<PostShort[]>([]);
  const loading = ref(false);
  computed(() =>
    postApi.getAll().then((res) => {
      posts.value = res;
      loading.value = true;
      return res;
    })
  );

  return {
    posts: computed<PostShort[]>(() => posts.value),
    postsLoading: computed<boolean>(() => loading.value)
  };
};
