import type { PostUpdateDto } from '~/dto/post-update.dto';
import type { PostCreateDto } from '~/dto/post-create.dto';
import type { Post } from '~/entry/post';

export class PostFormModel {
  id: null | number = null;

  title = '';

  description = '';

  imgUrl: '';

  static default(): PostFormModel {
    return new this();
  }

  static fromModel(post: Post): PostFormModel {
    const form = this.default();
    Object.assign(form, {
      id: post.id,
      title: post.title,
      description: post.descriptionShort,
      imgUrl: post.imgPreview
    });

    return form;
  }

  toDto(): PostUpdateDto | PostCreateDto {
    return {
      ...(this.id ? { id: this.id } : {}),
      imgPreview: this.imgUrl,
      title: this.title,
      descriptionShort: this.description
    };
  }
}
