export interface PostShort {
  id: number,
  imgPreview: string,
  title: string,
  descriptionShort: string,
}
