export interface PostPageContent {
  img: string,
  description: string,
  date: string,
}
