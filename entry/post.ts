import type { PostPageContent } from '~/entry/post-page-content';

export interface Post {
  id: number;
  imgPreview: string;
  title: string;
  descriptionShort: string;
  pageContent: PostPageContent;
  posts: Array<number>;
}
