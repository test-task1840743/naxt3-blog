import type { PostPageContent } from "~/entry/post-page-content";
import type { PostPageContentDto } from "~/dto/post-page-content.dto";

export const postPageContentFactory = {
  makeFromDto(dto: PostPageContentDto | null | undefined): PostPageContent {
    return {
      date: dto?.date || "Дата поста отсутствует",
      description: dto?.description || 'Описание отсутствует',
      img: dto?.img || ''
    };
  },
};
