import type { PostDto } from '~/dto/post.dto';
import type { Post } from '~/entry/post';
import { postPageContentFactory } from '~/factory/post-page-content.factory';

export const postFactory = {
  makeFromDto(dto: PostDto): Post {
    return {
      id: dto.id,
      descriptionShort: dto.descriptionShort || 'Нет краткого описания',
      imgPreview: dto.imgPreview || '',
      title: dto.title || 'Заголовок куда то делся',
      pageContent: postPageContentFactory.makeFromDto(dto.pageContent),
      posts: dto.posts || [dto.id]
    };
  }
};
