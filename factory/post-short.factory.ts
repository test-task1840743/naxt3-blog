import type { PostDto } from "~/dto/post.dto";
import type { PostShort } from "~/entry/post-short";

export const postShortFactory = {
  makeFromDto(dto: PostDto): PostShort {
    return {
      id: dto.id,
      descriptionShort: dto.descriptionShort || 'Нет краткого описания',
      imgPreview: dto.imgPreview || '',
      title: dto.title || 'Заголовок куда то делся',
    };
  },
};
