export interface PostPageContentDto {
  img: string | null | undefined,
  description: string | null | undefined,
  date: string | null | undefined,
}
