export interface PostCreateDto {
  imgPreview: string;
  title: string;
  descriptionShort: string;
}
