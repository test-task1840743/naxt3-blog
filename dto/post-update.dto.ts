export interface PostUpdateDto {
  id: number;
  imgPreview: string;
  title: string;
  descriptionShort: string;
}
