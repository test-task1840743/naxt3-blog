import type { PostPageContentDto } from '~/dto/post-page-content.dto';

export interface PostDto {
  id: number;
  imgPreview: string | null | undefined;
  title: string | null | undefined;
  descriptionShort: string | null | undefined;
  pageContent: null | undefined | PostPageContentDto;
  posts: null | undefined | Array<number>;
}
